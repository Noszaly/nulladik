function addBorder(pic)
  ans=["*"*s*"*" for s in pic ]
  cs=repeat('*',length(ans[1]))
  vcat(cs,ans,cs)
end
println(addBorder(split(readline())))
