function alternatingSums(a)
  [sum(a[1:2:end]),sum(a[2:2:end])]
end
println(alternatingSums(parse.(Int,split(readline()))))
