function reverseInParentheses(s::String)
  ans=[];nans=0
  op=zeros(Int,length(s));nop=0
  for i in 1:length(s)
    c=s[i]
    if c=='(' nop+=1;op[nop]=nans; continue end
    if c==')' 
      j=op[nop]; nop-=1
      reverse!(ans,j+1,nans)
      continue 
    end
    push!(ans, c); nans+=1
  end
  join(ans)
end
println(reverseInParentheses(readline()))