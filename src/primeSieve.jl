# kiszitálja az összetett számokat
function primeSieve(n)
  szita = fill(true,n)
  szita[4:2:n] .= false
  szita[9:6:n] .= false
  k, d = 5, 2
  while k*k <= n
    ( true == szita[k] ) ? ( szita[ 3k:2k:n ] .= false ) : nothing
    k += d
    d = 6 - d
  end
  szita
end
