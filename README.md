# Nulladik évfolyam
Matematika és programozás [Julia](julialang.org)-ban.

1. ## [Telepítés](null.md)
1. ## [Első](egy.md): Ismerkedés a Julia nyelvvel. Prímszámok.
1. ## [Második](ketto.md): Projecteuler-es feladatok.
1. ## [Harmadik](harom.md):Teljes indukció. Hátizsák feladat.
