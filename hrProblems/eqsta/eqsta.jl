function solve()
  n1,n2,n3=parse.(Int,split(readline()))
  f() = Set(cumsum(reverse(parse.(Int,split(readline())))))
  a1,a2,a3=f(),f(),f()
  metsz=intersect(a1,a2)
  if length(metsz)>0 metsz=intersect(metsz,a3) end
  ans=0
  if length(metsz)>0 
    ans=maximum(metsz) 
  end
  println(ans)
end

solve()