#include <sstream>
#include <vector>
#include <iostream>
using namespace std;

vector<int> parseInts(string str) {
  vector<int> ret;
  auto sstr=stringstream(str);
  string tok;
  while(getline(sstr,tok,',')){
    int a; stringstream(tok)>>a;
    ret.push_back(a);
  }
  return ret;
	// Complete this function
}

int main() {
    string str;
    cin >> str;
    vector<int> integers = parseInts(str);
    for(int i = 0; i < integers.size(); i++) {
        cout << integers[i] << "\n";
    }
    
    return 0;
}

