# Queues: A Tale of Two Stacks

let
  n=parse(Int,readline())
  que=zeros(Int,n); head,tail=1,1
  for _ in 1:n
    q=split(readline())
    t=parse(Int,q[1])
    if 1==t
      el=parse(Int,q[2])
      que[tail]=el; tail+=1
      continue
    end
    if 2==t
      head+=1
      continue
    end
    println(que[head])
  end
end