function solve(s) # rossz
  function ispal(ss,i,j)
    while i<j && ss[i]==ss[j]
      i+=1
      j-=1
    end
    i,j
  end
  i,j=ispal(s,1,length(s))
  if i>=j return -1 end
  
  ii,jj=ispal(s,i+1,j)
  if ii>=jj return i-1 end

  ii,jj=ispal(s,i,j-1)
  if ii>=jj return j-1 end
  -1
end

esetek=parse(Int,readline())
for _ in 1:esetek
  println(solve(readline()))
end
