function solve()
  n,k=parse.(Int,split(readline()))
  arr=parse.(Int,split(readline()))
  loc=zeros(Int,n)
  for i in 1:n
    loc[arr[i]]=i
  end

  i=1
  while k>0 && i<n
    if arr[i]!=n+1-i
      ii=loc[n+1-i]
      arr[ii]=arr[i]
      loc[arr[i]]=ii
      arr[i]=n+1-i
      loc[n+1-i]=i
      k-=1
    end
    i+=1
  end

  print(arr[1])
  for i in 2:n
    print(" ",arr[i])
  end
  println()
end

solve()