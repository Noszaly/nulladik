function solve()

  n=parse(Int,readline())
  arr=parse.(Int,split(readline()))
  mu=round(sum(arr)/n, digits=1)
  sort!(arr)
  n2=div(n,2)
  med=arr[n2+1]/1.0
  if 0==mod(n,2) med=(med+arr[n2])/2.0 end
  med=round(med, digits=1)

  arr=vcat(arr,arr[n]+1)
  ov,ofv=0,0
  v,fv=arr[1],1
  for i=2:n+1
    if arr[i]>v
      if fv>ofv 
        ov,ofv=v,fv
      end
      v,fv=arr[i],1
    else
      fv+=1
    end
  end
  modv=ov

  println(mu)
  println(med)
  println(modv)
end
solve()