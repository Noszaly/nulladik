# Pairs , interview

let
  n,k=parse.(Int,split(readline()))
  arr=parse.(Int,split(readline()))
  d=Dict{Int,Int}()
  ans=0
  for v in arr
    ans+=if haskey(d,v-k) d[v-k] else 0 end
    ans+=if haskey(d,v+k) d[v+k] else 0 end
    d[v]=if haskey(d,v) 1+d[v] else 1 end
  end
  println(ans)
end
