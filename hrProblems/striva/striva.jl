function solve()
  T=parse(Int,readline())
  for _ in 1:T
    sztr=readline()
    nq,pc,v=0,-1,true
    for c in sztr
      if isdigit(c)
        ac=parse(Int,c)
        if pc<0 pc=ac; nq=0; continue end
        if ac+pc != nq v=false; break end
        pc=ac
        nq=0 
        continue
      end
      if c=='?' nq+=1 end
    end
    println(if v "Valid" else "Invalid" end)
  end
end
solve()