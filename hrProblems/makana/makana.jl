function solve(a,b)
  function freq(s)
    f=zeros(Int,128)
    for si in s
      f[Int(si)]+=1
    end
    f
  end
  sum(abs.(freq(a) .- freq(b)))
end

println(solve(readline(),readline()))