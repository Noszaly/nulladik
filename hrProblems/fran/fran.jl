function solve()
  n,d=parse.(Int,split(readline()))
  nums=parse.(Int,split(readline()))

  fr=zeros(Int,201)
  for i in 1:d
    fr[nums[i]+1]+=1
  end
  d2,r2=divrem(d,2)
  d2+=r2
  function med2()
    s,i=0,1
    while i<=201
      s+=fr[i]
      if s>=d2 break end
      i+=1
    end
    ret=i-1
    if s>d2 || r2==1 
      ret+=(i-1)
    else
      i+=1
      while 0==fr[i]
        i+=1
      end
      ret+=(i-1)
    end
    ret
  end
  nfd=0
  for i in d+1:n
    m2=med2()
    a=nums[i]
    if a>=m2 nfd+=1 end
    fr[a+1]+=1
    fr[nums[i-d]+1]-=1
  end
  println(nfd)
end
solve()