using DelimitedFiles
let
n,d = map(x->parse(Int,x),split(readline()))
a = vec(readdlm(stdin,Int))
h = cld(d,2)
getmedian(v)=v[h]
!isodd(d) ? getmedian(v) = (v[h] + v[h+1]) / 2 : nothing
p = sort(a[1:d])
notifications = 0
for i = d+1:n
    if a[i] >= 2getmedian(p)
        notifications += 1
    end
    deleteat!(p,searchsortedfirst(p,a[i-d]))
    insert!(p,searchsortedfirst(p,a[i]),a[i])
end
print(notifications)
end