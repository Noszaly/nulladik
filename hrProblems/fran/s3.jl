let
# Enter your code here 
function parse_input()
    return [parse(Int, x) for x in split(readline())]
end

Z = 200

n, d = parse_input()
expenditures = parse_input()

dist = zeros(Int, Z+1)
for i=1:d
    dist[expenditures[i] + 1] += 1
end

med_at_odd = fld(d, 2) + 1
function med_is_lte_odd(val)
    s = 0
    for i=0:Z
        s += dist[i+1]
        if s >= med_at_odd || i > val
            return (i <= val)
        end
    end
end

med_at_even = fld(d,2)
function med_is_lte_even(val)
    s = 0
    for i=0:Z
        s += dist[i+1]
        if s >= med_at_even
            if s == med_at_even
                for j=(i+1):Z
                    if dist[j+1] > 0
                        return ((i + j)/2 <= val)
                    end
                end
            else
                return (i <= val)
            end
        end
    end
end


med_is_lte = med_is_lte_odd
if d % 2 == 0
    med_is_lte = med_is_lte_even
end
    
notifications = 0
for i=(d+1):n      
    if med_is_lte(expenditures[i] / 2)
        notifications += 1
    end
        
    dist[expenditures[i] + 1] += 1
    dist[expenditures[i-d] + 1] -= 1    
end

println(notifications)
end
