function ND(x)
  f(z)=exp(-0.5*z*z)
  ans=0.5
  if abs(x)<1e-12 return ans end
  neg=false
  if x<0.0 x=-x; neg=true end
  M=100
  h=x/M
  ans+=h/6.0*(2*sum(f.(h*(1:(M-1)))) + 1 + f(x) + 4* sum(f.(h*(0.5:M))))/sqrt(2.0*pi)
  if true==neg 1.0-ans else ans end
end
m,s=parse.(Float64,split(readline()))
u=parse(Float64,readline())
a,b=parse.(Float64,split(readline()))
u=(u-m)/s
println(round(ND(u),digits=3))
a,b=(a-m)/s,(b-m)/s
println(round(ND(b)-ND(a),digits=3))


