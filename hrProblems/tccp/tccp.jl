function cc(T, v)
  x=fill(0,T+1)
  x[1]=1
  for vi in v
    for i in (vi+1):(T+1)
      x[i]+=x[i-vi]
    end
  end
  x
end

t,nv=parse.(Int, split(readline()))
v=parse.(Int, split(readline()))
# println(t," ",nv)
# println(v)
println(cc(t,v)[t+1])

