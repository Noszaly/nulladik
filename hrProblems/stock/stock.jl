let
  T=parse(Int,readline())
  for _ in 1:T
    n=parse(Int,readline())
    p=parse.(Int,split(readline())) # prices
    lpf=trues(n)  # are there a larger price in the future ?
    lpf[n]=false
    mx=p[n]
    for i in (n-1):-1:1
      if p[i]==mx continue end
      if p[i]>mx lpf[i]=false; mx=p[i]; end
    end
    osszeg,darab,profit=0,0,0
    for i in 1:n
      if lpf[i]==true 
        osszeg+=p[i]
        darab+=1
      else
        profit+=( p[i]*darab - osszeg )
        osszeg,darab=0,0
      end
    end
    println(profit)
  end
end
