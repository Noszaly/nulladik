function solve()
  a=zeros(Int,1024)
  frek=zeros(Int,61)
  numOdd=0
  n=0
  ans=0

  function belso(i) # ezt kiszedtem mert lassitott
    for j in (i+1):n
      frek[a[j]]+=1
      numOdd+=if 1==frek[a[j]]%2 1 else -1 end
      if 0==numOdd || (1==numOdd && 1==(j-i+1)%2) ans+=1 end
    end
  end

  T=parse(Int,readline())
  for _ in 1:T
    n=parse(Int,readline())
    for i in 1:n
      a[i]=parse(Int,readline())
    end
    ans=n
    for i in 1:n
      frek .= 0
      frek[a[i]]=1
      numOdd=1
      belso(i)
      # for j in (i+1):n
      #   frek[a[j]]+=1
      #   numOdd+=if 1==frek[a[j]]%2 1 else -1 end
      #   if 0==numOdd || (1==numOdd && 1==(j-i+1)%2) ans+=1 end
      # end
    end
    println(ans)
  end
end
solve()