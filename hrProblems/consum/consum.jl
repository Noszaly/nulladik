function solve()
  fr=zeros(Int,128) # gyakorisagok
  T=parse(Int,readline())
  for _ in 1:T
    n,k=parse.(Int,split(readline()))
    x=mod.(cumsum(parse.(Int,split(readline()))), k) .+ 1
    fr[1:k] .= 0
    for v in x
      fr[v]+=1
    end
    ans=0
    n2(x)=div(x*(x-1),2)
    println(fr[1]+sum(n2.(fr[1:k])))
  end
end
solve()