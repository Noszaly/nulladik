# binary indexed tree, Fenwicck tree
function Bit(N,v=0) # 1..N
  tree=fill(v,N)
  function init()
    fill!(tree,v)
  end
  function query(x)
    ans=0
    while x>0
      ans+=tree[x]
      x-=(x&(-x))
    end
    ans
  end
  function update(x,val=1)
    while x<=N
      tree[x]+=val
      x+=(x&(-x))
    end
  end
  init,query,update,tree
end


let 
  na=5
  a=[1,2,3,4,5]
  init,query,update,tree=Bit(na)
  init()
  ans=0
  for i in 1:na
    ai=a[i]
    ans+=(i-1-query(ai))
    update(ai)
  end
  println(ans)
end