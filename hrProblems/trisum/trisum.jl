# Triple sum
let
  nA,nB,nC=parse.(Int,split(readline()))
  A=parse.(Int,split(readline()))
  B=parse.(Int,split(readline()))
  C=parse.(Int,split(readline()))
  sort!(unique!(A))
  sort!(unique!(B))
  sort!(unique!(C))
  all=vcat([(a,0) for a in A], [(b,2) for b in B], [(c,1) for c in C])
  sort!(all)
#println(all)  
  nA,nB,ans=0,0,0;
  for v in all
#println(v)    
    if v[2]==0 nA+=1; continue end
    if v[2]==1 nB+=1; continue end
    ans+=nA*nB
  end
  println(ans)
end