function solve(s)
  function freq(a)
    f=zeros(Int,128)
    for ai in a 
      f[Int(ai)]+=1 
    end
    f
  end

  f=freq(s)
  hs=length(s)
  nodd = sum(mod.(f, 2))
  nodd==0 || (nodd==1 && 1== mod(hs,2))
end

if solve(readline())
  println("YES")
else
  println("NO")
end