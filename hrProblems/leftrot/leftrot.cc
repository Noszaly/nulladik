# arrays left rotation
#include <bits/stdc++.h>

using namespace std;
int main()
{
  int n,d; cin>>n>>d;
  vector<string> arr(n+1);
  for( int i=1;i<=n;i++){ cin>>arr[i];}
  d=d%n;
  bool volt=false;
  for(int i=d+1;i<=n;i++){
    if(volt){ cout<<" ";volt=true; }else{volt=true;}
    cout<<arr[i];
  }
  for(int i=1;i<=d;i++){
    cout<<" "<<arr[i];
  }
  cout<<"\n";
  return 0;      
}
