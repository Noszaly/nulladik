# count triplets

let
  n,q=parse.(Int,split(readline()))
  arr=parse.(Int,split(readline()))
  L,R=Dict{Int,Int}(),Dict{Int,Int}()
  for v in arr
    R[v] = if haskey(R,v) R[v]+1 else 1 end
  end
  R[arr[1]]-=1; 
  ans=0
  for i in 2:n-1
    L[arr[i-1]]=if haskey(L,arr[i-1]) L[arr[i-1]]+1 else 1 end
    R[arr[i]]-=1; 
    a1=arr[i]
    if mod(a1,q)>0 continue end
    a0=div(a1,q)
    f0=if haskey(L,a0) L[a0] else 0 end
    if f0==0 continue end
    a2=a1*q
    f2=if haskey(R,a2) R[a2] else 0 end
    ans+=f0*f2
  end
  println(ans)
end
