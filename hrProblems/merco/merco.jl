function djSet(H)
  holvan=zeros(Int,H)
  meret=zeros(Int,H)
  function djInit()
    holvan .= -1
    meret .= 1
  end
  function djFind(t)
    ans=ht=t;
    while (ht=holvan[ht])>=0
      ans=ht
    end
    while (ht=holvan[t])>=0
      holvan[t]=ans;
      t=ht;
    end
    ans,meret[ans]
  end
  function djUnio(a,b)
    a,ma=djFind(a)
    b,mb=djFind(b)
    if(a!=b)
      if ma>mb a,b=b,a; ma,mb=mb,ma end
      holvan[a]=b
      meret[b]+=meret[a]
    end
  end
  djInit,djFind,djUnio
end

function solve()
  n, q=parse.(Int,split(readline()))
  mInit,mFind,mUnio=djSet(n)
  mInit()
  for i in 1:q
    ll=split(readline())
    if 2==length(ll)
      a=parse(Int,ll[2])
      a,sa=mFind(a)
      println(sa)
    else
      a,b=parse.(Int,ll[2:3])
      mUnio(a,b)
    end
  end
end
solve()