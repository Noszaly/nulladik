# Maximum Subarray Sum

function solve(arr,m)
  mx,mn,s=-1,m+1,0
  for i in 1:length(arr)
    s=(s+arr[i])%m
#println(i," ",s)
    if s>mx mx=s end
    if s>mn mx=max(mx,s-mn) end
    if s<mn mn=s end
  end
  mx
end

let
  Q=parse(Int,readline())
  for _ in 1:Q
    n,m=parse.(Int,split(readline()))
    arr=( parse.(Int,split(readline())) .% m )
    mx1=solve(arr,m)
    reverse!(arr)
    mx2=solve(arr,m)
    println(max(mx1,mx2))
  end
end
