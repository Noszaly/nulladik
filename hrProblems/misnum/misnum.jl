function solve()
  na=parse(Int,readline())
  a=parse.(Int,split(readline()))
  nb=parse(Int,readline())
  b=parse.(Int,split(readline()))
  mb=minimum(b)-1
  b = b .- mb
  a = a .- mb
  function freq(s,N)
    f=zeros(Int,N)
    for si in s
      f[Int(si)]+=1
    end
    f
  end

  L=128
  fa = freq(a,L)
  fb = freq(b,L)
  volt=false
  for i in 1:L
    if fa[i]<fb[i]
      if volt print(" ") end
      print(i+mb)
      volt=true
    end
  end
  if volt println() end
end
solve()