function spec(opt)
  update(x)=0<x<opt ? opt=x : nothing
  result()=opt
  update,result
end

function MSort(a)
  na=length(a)
  b=zeros(typeof(a[1]),na)

  upd,res=spec(typemax(Int))

  function merge(lo,m,up)
#println(lo," ",m," ",up)
    if up-lo<=0 return end
    if up-lo==1
      if a[lo]>a[up]
        a[lo],a[up]=a[up],a[lo]
        upd(a[lo]-a[up])      
      end
      return
    end
    ilo,iup,i=lo,m+1,lo
    while ilo<=m && iup<=up
      if a[ilo]>a[iup]
        upd(a[ilo]-a[iup])
        b[i]=a[iup]; i+=1; iup+=1
      else
        b[i]=a[ilo]; i+=1; ilo+=1
      end
    end
    while ilo<=m 
      b[i]=a[ilo]; i+=1; ilo+=1
    end
    while iup<=up 
      b[i]=a[iup]; i+=1; iup+=1
    end
    for i in lo:up a[i]=b[i] end
  end


  function msort(lo,up)
#println(lo," ",up)
    m=div(up+lo,2)
    if up-lo>0
      msort(lo,m)
      msort(m+1,up)
    end
    merge(lo,m,up)
  end

  msort,res
end


na=parse(Int,readline())
a=parse.(Int,split(readline()))

# a=[20, 7, 8, 2, 1]
# na=length(a)
rend,res=MSort(a)
rend(1,na)
println(res())
#println(a)
