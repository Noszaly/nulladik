let
  n=parse(Int,readline())
  d=Dict{String,String}()
  for _ in 1:n
  name, phone=split(readline())
  d[name]=phone
  end
  while true
    q=readline()
    if 0==length(q) break end
    if haskey(d, q )
      println(q,"=", d[q])
    else
      println("Not found")
    end
  end
end
