function solve()
  sa=Int8.(collect(readline()))
  sb=Int8.(collect(readline()))
println(stderr,length(sa)," ",length(sb))  
  function van(s,N=128)
    v=fill(false,N)
    for si in s
      v[Int(si)]=true
    end
    v
  end
  v = van(sa) .* van(sb) 
  a=vcat(-1,[c for c in sa if v[c]])
  b=vcat(-2,[c for c in sb if v[c]])
  la,lb=length(a),length(b)
println(stderr,length(a)," ",length(b))  
  ans=0
  
  if la>0 && lb>0
    prev,akt=zeros(Int16,lb),zeros(Int16,lb)
    function belso(ai,j)
      if ai==b[j]
        akt[j]=1+prev[j-1]
      else
        akt[j]=max(akt[j-1],prev[j])
      end
    end
    for i in 2:la
      prev,akt=akt,prev
      akt[1]=0
      ai=a[i]
      for j in 2:lb
        belso(ai,j)
        # if ai==b[j]
        #   akt[j]=1+prev[j-1]
        # else
        #   akt[j]=max(akt[j-1],prev[j])
        # end
      end
    end
    ans=akt[lb]
  end
  println(ans)
end
solve()