function solve()
  n=parse(Int,readline())
  lista=[]
  for i in 1:n
    x=parse.(Int,split(readline()))
    x[1]+=x[2]; x[2]=i
    push!(lista,x)
  end
  sort!(lista)
  print(lista[1][2])
  for i in 2:n
    print(" ",lista[i][2])
  end
  println()
end

solve()