function solve()
  n=parse(Int,readline())
  m=parse(Int,readline())
  mtx=fill(0,n,m)

  for i in 1:n
    mtx[i,:]=parse.(Int,split(readline()))
  end
  akt=0
  function bejar(i,j)
    if i<1||i>n||j<1||j>m return end
    if 0==mtx[i,j] return end
    mtx[i,j]=0
    akt+=1
    for di in -1:1, dj in -1:1
      if 0==di && 0==dj continue end
      bejar(i+di,j+dj)
    end
  end
  ans=0
  for i in 1:n, j in 1:m
    akt=0
    bejar(i,j)
    ans=max(ans,akt)
  end
  println(ans)
end
solve()